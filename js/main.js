$(document).ready(function(){
	$.ajax({
		url: 'http://alex.devel.softservice.org/testapi/',
		method: 'get',
		success: function(response){
			var balance = response.balance_usd;
			var targetReach = 15;
			var rate = 0.2;
			var timer;
			var growTimer = setInterval(growProgress, 20);

			$('.preload').fadeOut(200);
			$('.progress-target-bottom').text(targetReach + '$');

			$( "#progressbar" ).progressbar({
				max: targetReach,
				value: 0,
				complete: function(){
					clearInterval(timer);
					$('.progress-block').addClass('done');
				},
				change: function(){
					var str = '$' + progressValue;
					$('#progressTitle span').text(str);
				}
			});
			$('.ui-progressbar-value.ui-corner-left.ui-widget-header').append($('#progressTitle'));
			var progressValue = $( "#progressbar" ).progressbar( "option", "value" );

			function growProgress(){
				if(progressValue >= balance){
					clearInterval(growTimer);
					timer = setInterval(raiseProgress, 2000);
					$('#progressTitle').fadeIn(1000);
				}else{
					$("#progressbar").progressbar( "option", "value", ++progressValue);
				}
			}
			function raiseProgress(){
				var acc = 10;
				var result = (progressValue * acc) + (rate * acc);
				var needToComplete = (targetReach * acc) - result;
				progressValue = result / acc;
				$( "#progressbar" ).progressbar( "option", "value", progressValue);
				$('#needToComplete').text(needToComplete / acc);
			}
		},
		error: function(data){
			$('.preload').text('Mmm... Error ;(')
			console.log(data);
		}
	})
})